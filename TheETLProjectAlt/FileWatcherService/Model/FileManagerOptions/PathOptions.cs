﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Model.FileManagerOptions
{
    public class PathOptions : Options
    {
        [Path]
        public string SourceDirectory { get; set; } = "C:\\RK\\Source";
        [Path]
        public string TargetDirectory { get; set; } = "C:\\RK\\Target";
        
        public bool EnableArchivation { get; set; } = true;
        [Path]
        public string ArchiveDirectory { get; set; } = "C:\\RK\\Target\\_Archive";

        public PathOptions()
        {

        }
    }
}
