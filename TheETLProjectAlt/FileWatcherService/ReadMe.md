First of all, huge thanks to [@tsudd](https://github.com/tsudd) for letting me use his project to pass the assignment as well as for some assistance.

A quick breakdown:

_ConfigManager_ class facilitates the selection of some options either extracted from a .json/.xml file in the directory specified on _ConfigManager_ initialization, or the default ones.

_ConfigProvider_ class provides tools to extract data from .xml/.json files and to generate an instance of some model class from the Dictionary<string, object>.

_DataAccessLayer_ is the entity working directly with the SQL database. It contains model classes for the SQL database entities and the _DataAccess_ class which facilitates the extraction of data from the SQL database as instances of model classes using stored procedures of the database.

_ServiceLayerService_ class uses _DataAccess_ in a set of methods to get collections of model classes' instances from the database being basically some kind of a compact wrapper of _DataAccess_.

_DataManager_ project contains a class to tranfer files, a class to generate .xml files from some entity and the _DataManager_ console app for the user to run to extract data from their database with specified configurations. For the latter purpose it uses the _ServiceLayerService_ class.

_FileWatcherService_ is a Windows service that monitors some directory specified in a configuration file and moves newly created files from it with encryption and compression into specified target directory.
