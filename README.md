# 3rd-term-csharp-assignments

UPD: the 4th assignment is in _TheETLProjectAlt_

The executable, config.xml, and appsettings.json are in the _Job folder_. There are also setup.cmd and uninstall.cmd **to be run from the command prompt which is to be opened as the administrator**; these are for setting up the service and uninstalling it respectively (assuming InstallUtil.exe on yor PC is in C:\Windows\Microsoft.NET\Framework64\v4.0.30319).

The _Job folder_ can be placed wherever one likes. The ETL.exe in it is the same file as ETL.exe in the bin\Debug. Files config.xml and appsettings.json contain the paths of source folder, buffer folder, target folder, log file, and the password for AES encryption.

1. source folder is the folder being monitored by the service;

2. buffer folder is the "public" folder meaning the one where the "sender" places encrypted compessed file and the "recipient" gets it, decompresses it and decrypts it;

3. target folder is the folder with hierarchially arranged folders with the files moved from the source folder;

The service gets paths for all these 3 folders from config.xml or appsettings.json if the former is not available*. It does that when setting up; config.xml and appsettings.json are also used in the "service runtime" to encrypt and decrypt the files: when encrypting .xml is prioritized, when decrypting - the other one is.

*There's still a bug I can't fix - when removing config.xml the service can't use appsettings.json properly though the methods used in JsonParser work fine - I've tested them.
