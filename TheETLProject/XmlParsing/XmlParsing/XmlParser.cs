﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace XmlParsing
{
    /// <summary>
    /// Simple xml parser to use as a safe option.
    /// </summary>
    public class XmlParser
    {
        private readonly Dictionary<string, string> xmlElements;

        public XmlParser(string xmlDocPath)
        {
            if (xmlDocPath is null)
            {
                throw new ArgumentNullException(nameof(xmlDocPath));
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlDocPath);

            xmlElements = new Dictionary<string, string>();
            foreach (XmlNode node in doc.DocumentElement)
            {
                xmlElements.Add(node.Name, node.InnerText);
            }
        }

        public string GetElementValue(string key)
        {
            return xmlElements[key];
        }
    }
}
