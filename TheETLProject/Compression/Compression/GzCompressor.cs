﻿using System;
using System.IO;
using System.IO.Compression;

namespace Compression
{
    /// <summary>
    /// This class facilitates .gz compression of files
    /// </summary>
    /// <remarks>It should've been static imo</remarks>
    public class GzCompressor
    {
        /// <summary>
        /// The method compresses file at <paramref name="fileFullPath"/> path into .gz archive at <paramref name="compressedFileFullPath"/> path.
        /// </summary>
        /// <param name="fileFullPath">The path of the file to compress.</param>
        /// <param name="compressedFileFullPath">The unoccupied path to create the archive at.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if there are null arguments.</exception>
        /// <exception cref="System.ArgumentException">Thrown if <paramref name="fileFullPath"/> path is unoccupied by a file or if <paramref name="compressedFileFullPath"/> path is occupied by a file.</exception>
        public void CompressFile(string fileFullPath, string compressedFileFullPath)
        {
            if (fileFullPath is null)
            {
                throw new ArgumentNullException(nameof(fileFullPath));
            }

            if (compressedFileFullPath is null)
            {
                throw new ArgumentNullException(nameof(compressedFileFullPath));
            }

            if (!File.Exists(fileFullPath))
            {
                throw new ArgumentException("The file at the argument path does not exist.", nameof(fileFullPath));
            }

            if (File.Exists(compressedFileFullPath))
            {
                throw new ArgumentException("The argument path is already occupied by an existing file.", nameof(compressedFileFullPath));
            }

            var bytes = File.ReadAllBytes(fileFullPath);

            using FileStream fs = new FileStream(compressedFileFullPath, FileMode.CreateNew);
            using GZipStream zipStream = new GZipStream(fs, CompressionMode.Compress, false);
            zipStream.Write(bytes, 0, bytes.Length);
        }

        /// <summary>
        /// The method decompresses the .gz archive at <paramref name="compressedFileFullPath"/> path into file at <paramref name="decompressedFileFullPath"/> path.
        /// </summary>
        /// <param name="compressedFileFullPath">The path of the archive to decompress.</param>
        /// <param name="decompressedFileFullPath">The unoccupied path to create the decompressed file at.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if there are null arguments.</exception>
        /// <exception cref="System.ArgumentException">Thrown if <paramref name="compressedFileFullPath"/> path is unoccupied by a file or if <paramref name="decompressedFileFullPath"/> path is occupied by a file.</exception>
        public void DecompressFile(string compressedFileFullPath, string decompressedFileFullPath)
        {
            if (compressedFileFullPath is null)
            {
                throw new ArgumentNullException(nameof(compressedFileFullPath));
            }

            if (decompressedFileFullPath is null)
            {
                throw new ArgumentNullException(nameof(decompressedFileFullPath));
            }

            if (!File.Exists(compressedFileFullPath))
            {
                throw new ArgumentException("The file at the argument path does not exist.", nameof(compressedFileFullPath));
            }

            if (File.Exists(decompressedFileFullPath))
            {
                throw new ArgumentException("The argument path is already occupied by an existing file.", nameof(decompressedFileFullPath));
            }

            var compressedFile = new FileInfo(compressedFileFullPath);

            using FileStream originalFileStream = compressedFile.OpenRead();
            using FileStream decompressedFileStream = File.Create(decompressedFileFullPath);
            using GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress);
            decompressionStream.CopyTo(decompressedFileStream);
        }
    }
}
