﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace STJsonParsing
{
    /// <summary>
    /// System.Text.Json parser.
    /// </summary>
    public class STJsonParser
    {
        private readonly Dictionary<string, string> jsonElements;

        public STJsonParser(string jsonDocPath)
        {
            if (jsonDocPath is null)
            {
                throw new ArgumentNullException(nameof(jsonDocPath));
            }

            jsonElements = new Dictionary<string, string>();

            string jsonDocContents = File.ReadAllText(jsonDocPath);
            using JsonDocument doc = JsonDocument.Parse(jsonDocContents);
            JsonElement root = doc.RootElement;

            var props = root.EnumerateObject();
            while (props.MoveNext())
            {
                var prop = props.Current;
                jsonElements.Add(prop.Name, prop.Value.ToString());
            }
        }

        public string GetElementValue(string key)
        {
            return jsonElements[key];
        }
    }
}
