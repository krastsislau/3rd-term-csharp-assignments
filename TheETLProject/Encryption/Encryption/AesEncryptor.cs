﻿using System;
using System.Security.Cryptography;
using System.IO;

namespace Encryption
{
    // TODO: maybe remove the console writelines or replace it with something else

    /// <summary>
    /// The AesEncryptor class facilitates AES Encryption of files using the password assigned at initialization.
    /// </summary>
    /// <remarks>This class might 've been better implemented as static, and in fact it was - I've refactored it to be an instance class to meet the "no statics" requirement.
    /// The original implementation: https://ourcodeworld.com/articles/read/471/how-to-encrypt-and-decrypt-files-using-the-aes-encryption-algorithm-in-c-sharp </remarks>
    public class AesEncryptor
    {
        public string Password { get; }

        public AesEncryptor(string password)
        {
            this.Password = password;
        }

        /// <summary>
        /// Creates a random salt that will be used to encrypt the file. This method is required on FileEncrypt.
        /// </summary>
        /// <returns></returns>
        private byte[] GenerateRandomSalt()
        {
            byte[] data = new byte[32];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                for (int i = 0; i < 10; i++)
                {
                    // Fill the buffer with the generated data
                    rng.GetBytes(data);
                }
            }

            return data;
        }

        /// <summary>
        /// Encrypts a file from its path and a plain password.
        /// </summary>
        /// <param name="inputFilePath">Path occupied with the file to encrypt.</param>
        /// <param name="outputFilePath">Unoccupied path where the encrypted file is to be created.</param>
        /// <remarks>All parameter paths are to be full, meaning they should also include the file name.</remarks>
        public void FileEncrypt(string inputFilePath, string outputFilePath)
        {
            if (inputFilePath is null)
            {
                throw new ArgumentNullException(nameof(inputFilePath));
            }

            if (outputFilePath is null)
            {
                throw new ArgumentNullException(nameof(outputFilePath));
            }

            if (!File.Exists(inputFilePath))
            {
                throw new ArgumentException("The file to encrypt doesn't exist at the specified path.", nameof(inputFilePath));
            }

            if (File.Exists(outputFilePath))
            {
                throw new ArgumentException("The path where the encrypted file is to be created is already occupied.", nameof(outputFilePath));
            }

            // http://stackoverflow.com/questions/27645527/aes-encryption-on-large-files

            byte[] salt = GenerateRandomSalt();
            FileStream fsCrypt = new FileStream(outputFilePath, FileMode.Create);

            // Convert password string to byte arrray
            byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(this.Password);

            // Set Rijndael symmetric encryption algorithm
            RijndaelManaged AES = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128,
                Padding = PaddingMode.PKCS7
            };

            // http://stackoverflow.com/questions/2659214/why-do-i-need-to-use-the-rfc2898derivebytes-class-in-net-instead-of-directly
            // "What it does is repeatedly hash the user password along with the salt." High iteration counts.
            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);

            // Cipher modes: http://security.stackexchange.com/questions/52665/which-is-the-best-cipher-mode-and-padding-mode-for-aes-encryption
            // My note: the cipher mode used originally didn't work for me for some reason
            AES.Mode = CipherMode.CBC;

            // Write salt to the begining of the output file, so in this case can be random every time
            fsCrypt.Write(salt, 0, salt.Length);

            CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateEncryptor(), CryptoStreamMode.Write);

            FileStream fsIn = new FileStream(inputFilePath, FileMode.Open);

            // Create a buffer (1mb) so only this amount will allocate in the memory and not the whole file
            byte[] buffer = new byte[1048576];
            int read;

            try
            {
                while ((read = fsIn.Read(buffer, 0, buffer.Length)) > 0)
                {
                    cs.Write(buffer, 0, read);
                }

                // Close up
                fsIn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                cs.Close();
                fsCrypt.Close();
            }
        }

        /// <summary>
        /// Decrypts an encrypted file with the FileEncrypt method through its path and the plain password.
        /// </summary>
        /// <param name="inputFilePath">Path occupied with the file to decrypt.</param>
        /// <param name="outputFilePath">Unoccupied path where the decrypted file is to be created.</param>
        /// <remarks>All parameter paths are to be full, meaning they should also include the file name.</remarks>
        public void FileDecrypt(string inputFilePath, string outputFilePath)
        {
            if (inputFilePath is null)
            {
                throw new ArgumentNullException(nameof(inputFilePath));
            }

            if (outputFilePath is null)
            {
                throw new ArgumentNullException(nameof(outputFilePath));
            }

            if (!File.Exists(inputFilePath))
            {
                throw new ArgumentException("The file to decrypt doesn't exist at the specified path.", nameof(inputFilePath));
            }

            if (File.Exists(outputFilePath))
            {
                throw new ArgumentException("The path where the decrypted file is to be created is already occupied.", nameof(outputFilePath));
            }

            byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(this.Password);
            byte[] salt = new byte[32];

            FileStream fsCrypt = new FileStream(inputFilePath, FileMode.Open);
            fsCrypt.Read(salt, 0, salt.Length);

            RijndaelManaged AES = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128
            };

            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);
            AES.Padding = PaddingMode.PKCS7;
            AES.Mode = CipherMode.CBC;

            CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateDecryptor(), CryptoStreamMode.Read);

            FileStream fsOut = new FileStream(outputFilePath, FileMode.Create);

            int read;
            byte[] buffer = new byte[1048576];

            try
            {
                while ((read = cs.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fsOut.Write(buffer, 0, read);
                }
            }
            catch (CryptographicException ex_CryptographicException)
            {
                Console.WriteLine("CryptographicException error: " + ex_CryptographicException.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            try
            {
                cs.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error by closing CryptoStream: " + ex.Message);
            }
            finally
            {
                fsOut.Close();
                fsCrypt.Close();
            }
        }
    }
}
