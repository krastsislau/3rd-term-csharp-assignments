﻿using STJsonParsing;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using XmlParsing;

namespace ETL
{
    static class Program
    {
        private const string XmlConfigurationFileName = "config.xml";
        private const string JsonConfigurationFileName = "appsettings.json";

        private static readonly string ExeFolderPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        // these are public cause I use them in the FileTransporter class in the service runtime
        public static readonly string XmlConfigPath = Path.Combine(ExeFolderPath, XmlConfigurationFileName);
        public static readonly string JsonConfigPath = Path.Combine(ExeFolderPath, JsonConfigurationFileName);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            XmlParser startupXmlParser;
            try
            {
                startupXmlParser = new XmlParser(XmlConfigPath);
            }
            catch
            {
                startupXmlParser = null;
            }

            STJsonParser startupJsonParser;
            try
            {
                startupJsonParser = new STJsonParser(JsonConfigPath);
            }
            catch
            {
                startupJsonParser = null;
            }

            // TODO: indicate in some way that the service setup has failed due to the config errors
            if (startupXmlParser is null && startupJsonParser is null)
            {
                return;
            }

            // at this point one or both of the parsers has been constructed and are not null so let's try using the xml one first, than the json one

            string source, buffer, target, logs;

            if (!(startupXmlParser is null))
            {
                source = startupXmlParser.GetElementValue("SourceFolderPath");
                buffer = startupXmlParser.GetElementValue("BufferFolderPath");
                target = startupXmlParser.GetElementValue("TargetFolderPath");
                logs = startupXmlParser.GetElementValue("LogFileFullPath");
            }
            else
            {
                source = startupJsonParser.GetElementValue("SourceFolderPath");
                buffer = startupJsonParser.GetElementValue("BufferFolderPath");
                target = startupJsonParser.GetElementValue("TargetFolderPath");
                logs = startupJsonParser.GetElementValue("LogFileFullPath");
            }

            // a little folder validation
            if (source == buffer || source == target || buffer == target)
            {
                return;
            }

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ETL(source, buffer, target, logs),
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
