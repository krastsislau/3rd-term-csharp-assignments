﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace ETL
{
    [RunInstaller(true)]
    public partial class ETLInstaller : Installer
    {
        ServiceInstaller serviceInstaller;
        ServiceProcessInstaller processInstaller;

        public ETLInstaller()
        {
            InitializeComponent();
            serviceInstaller = new ServiceInstaller();
            processInstaller = new ServiceProcessInstaller();

            processInstaller.Account = ServiceAccount.LocalSystem;
            serviceInstaller.StartType = ServiceStartMode.Manual;
            serviceInstaller.ServiceName = "ETLServiceByKRastsislau_1";
            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);
        }
    }
}
