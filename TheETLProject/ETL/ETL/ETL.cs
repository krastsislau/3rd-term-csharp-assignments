﻿using System;
using System.ServiceProcess;
using System.Threading;

namespace ETL
{
    public partial class ETL : ServiceBase
    {
        public string LogFileFullPath { get; }
        public string SourceFolderPath { get; }
        public string BufferFolderPath { get; }
        public string TargetFolderPath { get; }

        private FileTransporter fileTansporter;

        public ETL(string sourceFolderPath, string bufferFolderPath, string targetFolderPath, string logFileFullPath)
        {
            if (sourceFolderPath is null)
            {
                throw new ArgumentNullException(nameof(sourceFolderPath));
            }

            if (bufferFolderPath is null)
            {
                throw new ArgumentNullException(nameof(bufferFolderPath));
            }

            if (targetFolderPath is null)
            {
                throw new ArgumentNullException(nameof(targetFolderPath));
            }

            if (logFileFullPath is null)
            {
                throw new ArgumentNullException(nameof(logFileFullPath));
            }

            InitializeComponent();
            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;

            this.SourceFolderPath = sourceFolderPath;
            this.BufferFolderPath = bufferFolderPath;
            this.TargetFolderPath = targetFolderPath;

            this.LogFileFullPath = logFileFullPath;
        }

        protected override void OnStart(string[] args)
        {
            this.fileTansporter = new FileTransporter(this.SourceFolderPath, this.BufferFolderPath, this.TargetFolderPath, this.LogFileFullPath);
            Thread fileTansporterThread = new Thread(new ThreadStart(this.fileTansporter.Start));
            fileTansporterThread.Start();
        }

        protected override void OnStop()
        {
            this.fileTansporter.Stop();
            Thread.Sleep(1000);
        }
    }
}
