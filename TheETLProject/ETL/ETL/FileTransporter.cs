﻿using System;
using System.IO;
using System.Threading;
using Compression;
using Encryption;
using STJsonParsing;
using XmlParsing;

namespace ETL
{
    class FileTransporter
    {
        #region Fields

        private readonly FileSystemWatcher sourceFolderWatcher;
        private readonly object obj = new object();
        private bool enabled = true;

        #endregion

        #region Properties

        public string SourceFolderPath { get; }
        public string BufferFolderPath { get; }
        public string TargetFolderPath { get; }
        public string LogFileFullPath { get; }

        #endregion

        #region Constructor and Finalizer

        public FileTransporter(string sourceFolderPath, string bufferFolderPath, string targetFolderPath, string logFileFullPath)
        {
            if (sourceFolderPath is null)
            {
                throw new ArgumentNullException(nameof(sourceFolderPath));
            }

            if (bufferFolderPath is null)
            {
                throw new ArgumentNullException(nameof(bufferFolderPath));
            }

            if (targetFolderPath is null)
            {
                throw new ArgumentNullException(nameof(bufferFolderPath));
            }

            if (logFileFullPath is null)
            {
                throw new ArgumentNullException(nameof(logFileFullPath));
            }

            if (!Directory.Exists(sourceFolderPath))
            {
                Directory.CreateDirectory(sourceFolderPath);
            }

            if (!Directory.Exists(bufferFolderPath))
            {
                Directory.CreateDirectory(bufferFolderPath);
            }

            if (!Directory.Exists(targetFolderPath))
            {
                Directory.CreateDirectory(targetFolderPath);
            }

            this.SourceFolderPath = sourceFolderPath;
            this.BufferFolderPath = bufferFolderPath;
            this.TargetFolderPath = targetFolderPath;
            this.LogFileFullPath = logFileFullPath;

            sourceFolderWatcher = new FileSystemWatcher(sourceFolderPath);

            sourceFolderWatcher.Changed += SourceFolderWatcher_Changed;
            sourceFolderWatcher.Created += SourceFolderWatcher_Created;
            sourceFolderWatcher.Deleted += SourceFolderWatcher_Deleted;
            sourceFolderWatcher.Renamed += SourceFolderWatcher_Renamed;

            WriteToLog($"SERVICE: {DateTime.Now:dd/MM/yyyy hh:mm:ss} a FileTransporter instance created.");
        }

        ~FileTransporter() => WriteToLog($"SERVICE: {DateTime.Now:dd/MM/yyyy hh:mm:ss} a FileTransporter instance finalized.");

        #endregion

        #region Watcher Start & Stop

        public void Start()
        {
            sourceFolderWatcher.EnableRaisingEvents = true;
            while (enabled)
            {
                Thread.Sleep(1000);
            }

            WriteToLog($"SERVICE: {DateTime.Now:dd/MM/yyyy hh:mm:ss} the watcher is monitoring the source folder now.");
        }

        public void Stop()
        {
            sourceFolderWatcher.EnableRaisingEvents = false;
            enabled = false;

            WriteToLog($"SERVICE: {DateTime.Now:dd/MM/yyyy hh:mm:ss} the watcher has stopped monitoring the source folder.");
        }

        #endregion

        #region SourceFolderWatcher Methods

        private void SourceFolderWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            string fileEvent = "changed";
            string filePath = e.FullPath;
            RecordEntry(fileEvent, filePath);

            FileInSourceCreatedOrChanged(e);
        }

        private void SourceFolderWatcher_Created(object sender, FileSystemEventArgs e)
        {
            string fileEvent = "created";
            string filePath = e.FullPath;
            RecordEntry(fileEvent, filePath);

            FileInSourceCreatedOrChanged(e);
        }

        private void SourceFolderWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            string fileEvent = "deleted";
            string filePath = e.FullPath;
            RecordEntry(fileEvent, filePath);
        }

        private void SourceFolderWatcher_Renamed(object sender, RenamedEventArgs e)
        {
            string fileEvent = "renamed to" + e.FullPath;
            string filePath = e.OldFullPath;
            RecordEntry(fileEvent, filePath);
        }

        #endregion

        private void FileInSourceCreatedOrChanged(FileSystemEventArgs e)
        {
            string loggingOffset = "\t";
            lock (obj)
            {
                // setting up the parser(s) to get the password
                XmlParser serviceRuntimeXmlParser;
                try
                {
                    serviceRuntimeXmlParser = new XmlParser(Program.XmlConfigPath);
                }
                catch
                {
                    serviceRuntimeXmlParser = null;
                }

                STJsonParser serviceRuntimeJsonParser;
                try
                {
                    serviceRuntimeJsonParser = new STJsonParser(Program.JsonConfigPath);
                }
                catch
                {
                    serviceRuntimeJsonParser = null;
                }

                if (serviceRuntimeXmlParser is null && serviceRuntimeJsonParser is null)
                {
                    WriteToLog("???: both configuration files are inaccessable for some reason - file transportation aborted.");
                    return;
                }

                // so one or both of the parsers are not null

                // copy the file from source to target
                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} Copying file to target...");    
                
                string fileInTargetFullPath = GenerateFreeName(Path.Combine(this.TargetFolderPath, e.Name));
                File.Copy(e.FullPath, fileInTargetFullPath);

                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} ...done.");

                // Optional TODO: clear the password from memory asap after it's been used
                // encrypt the file in target
                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} Encrypting file in target...");

                string passwordToEncryptWith;
                if (!(serviceRuntimeXmlParser is null))
                {
                    passwordToEncryptWith = serviceRuntimeXmlParser.GetElementValue("password");
                }
                else
                {
                    passwordToEncryptWith = serviceRuntimeJsonParser.GetElementValue("password");
                }

                string encryptedFileInTargetFullPath = GenerateFreeName(fileInTargetFullPath + ".aes");
                AesEncryptor encryptor = new AesEncryptor(passwordToEncryptWith);

                encryptor.FileEncrypt(fileInTargetFullPath, encryptedFileInTargetFullPath);

                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} ...done.");

                // archive filename_numberIfNessesary.aes into filename_numberIfNessesary.aes.gz
                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} Compressing file in target...");

                string gzCompressedEncryptedFileInTargetFullPath = GenerateFreeName(encryptedFileInTargetFullPath + ".gz");
                new GzCompressor().CompressFile(encryptedFileInTargetFullPath, gzCompressedEncryptedFileInTargetFullPath);

                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} ...done.");

                // copy file.aes.gz into buffer
                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} Moving file to buffer...");

                string gzCompressedEncryptedFileInBufferFullPath = GenerateFreeName(Path.Combine(this.BufferFolderPath, new FileInfo(gzCompressedEncryptedFileInTargetFullPath).Name));
                File.Move(gzCompressedEncryptedFileInTargetFullPath, gzCompressedEncryptedFileInBufferFullPath);

                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} ...done.");

                // delete file and file.aes in target
                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} Deleting original file and the encrypted one in target...");

                File.Delete(fileInTargetFullPath);
                File.Delete(encryptedFileInTargetFullPath);

                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} ...done.");

                // decompress file.aes.gz into file.aes
                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} Decompressing the .gz file in target into encrypted file in target...");

                encryptedFileInTargetFullPath = GenerateFreeName(encryptedFileInTargetFullPath);
                new GzCompressor().DecompressFile(gzCompressedEncryptedFileInBufferFullPath, encryptedFileInTargetFullPath);

                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} ...done.");

                // decrypt file.aes in target into the datetime folder of the day as hh-mm-ss
                // Optional TODO: clear the password from memory asap after it's been used
                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} Decrypting the decompressed file and placing it into the date folder...");

                string archivedFileFullDatePath = Path.Combine(this.TargetFolderPath, DateTime.Now.ToString("yyyy/MM/dd"));
                if (!Directory.Exists(archivedFileFullDatePath))
                {
                    Directory.CreateDirectory(archivedFileFullDatePath);
                }

                string passwordToDecryptWith;
                if (!(serviceRuntimeJsonParser is null))
                {
                    passwordToDecryptWith = serviceRuntimeJsonParser.GetElementValue("password");
                }
                else
                {
                    passwordToDecryptWith = serviceRuntimeXmlParser.GetElementValue("password");
                }

                AesEncryptor decryptor = new AesEncryptor(passwordToDecryptWith);
                decryptor.FileDecrypt(encryptedFileInTargetFullPath, Path.Combine(archivedFileFullDatePath, DateTime.Now.ToString("HH-mm-ss")));

                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} ...done.");

                // delete file.aes in target
                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} Deleting the encrypted file in target...");

                File.Delete(encryptedFileInTargetFullPath);

                WriteToLog(loggingOffset + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss} ...done.");
            }
        }

        private static string GenerateFreeName(string filePath)
        {
            if (filePath is null)
            {
                throw new ArgumentNullException(nameof(filePath));
            }

            if (!File.Exists(filePath))
            {
                return filePath;
            }

            for (int i = 1; i < int.MaxValue ; i++)
            {
                string attemptedName = filePath + i.ToString("X");
                if (!File.Exists(attemptedName))
                {
                    return attemptedName;
                }
            }

            // TODO: handle this some other way
#pragma warning disable S112 // Unused private types or members should be removed
            throw new Exception("Failed to generate free name: that wasn't really expected.");
#pragma warning restore S112 // Unused private types or members should be removed
        }

        #region Methods for Logging

        private void RecordEntry(string fileEvent, string filePath)
        {
            WriteToLog(string.Format("UPD: {0} file {1} was {2}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"), filePath, fileEvent));
        }

        private void WriteToLog(string message)
        {
            lock (obj)
            {
                using StreamWriter writer = new StreamWriter(this.LogFileFullPath, true);
                writer.WriteLine(message);
                writer.Flush();
            }
        }

        #endregion
    }
}
